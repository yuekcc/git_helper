# Git Helper

Git Helper 是一个面向双远程仓库的 git 辅助工具。

## 双远程仓库工作流

公司配置的是一个类似于 gitlab 的 git 平台。因为项目管理上的需要。日常开发中需要使用两个远程仓库。一个称为中心仓库，项目属于代码平台的一个组织；另一个则是个人的 fork 仓库。所有开发工作都要在个人 fork 仓库完成，然后通过 merge request 合并到中心仓库。

所以日常的工作流程基本上可以总结为这样：

1. 先从中心仓库拉出新的特性或 bugfix 分支：feat_branch
2. 修改代码
3. 推送 feat_branch 分支到个人 fork 仓库
4. 在中心仓库的控制台页面上发起 merge request，源是 fork 仓库 的 feat_branch 分支，目标是中心仓库的 pre_release 分支
   - 此时会进行 code review
5. 通过 ci/cd 系统，部署 pre_release 分支
6. 测试
   - 如果有 bug 需要修改，首先是先 pull 中心仓库的 pre_release 分支到 feat_branch（因为可能有其他开发的代码已经合并到 pre_release，同步一次减少冲突）
   - 跳到上面的第 2 步
7. 测试通过后，再在中心仓库的控制台页面上发起 merge request，将 pre_release 分支合并到 release 分支
8. 上线发布

![workflow](multi-remote-workflow.svg)

换为 git 命令操作，大概就是这些：

```sh
# 项目总是从中心仓库 clone
git clone https://mygitlab.com/org_name/project_name

# 将 origin 设置为 fork 仓库
# 后续提交时，只能权限上传到 fork 仓库
git remote set-url origin https://mygitlab.com/user_name/project_name
# 增加 upstream 上游，指向中心仓库，方便更新代码
git remote add upstream https://mygitlab.com/org_name/project_name

# 更新所有远程仓库的数据
git fetch --all --tags # 获取所有远程仓库的更新

# 从中心仓库的 pre_release 分支拉出特性分支，用于功能开发
# 注意此时个人的 fork 仓库是没有 feat_branch 分支
git checkout -b feat_branch upstream/pre_release

# 编辑代码
vi .

# 保存修改
git add .
git commit -m "make some changed"

# 将代码推送到个人 fork 仓库
git push --set-upstream origin feat_branch


# 如果有其他人的代码修改，需要再次同步代码，此时应该同步 upstream 中代码
# 其他协作的开发，代码也应该要合并到 upstream/dev 分支
git pull upstream/pre_release

# 因为上面的已经设置过 --set-upstream，此时直接 push 会推送代码到 origin
git push # 实际效果是 git push origin feat_branch
```

代码推送到 origin 后，此时得回到代码平台页面，发起 merge request 流程。

## Git Helper 的改进

由上面的命令可以看出，双远程仓库的工作流中需要多次切换到远程仓库、设置远程仓库。Git helper 改进的核心便减少远程仓库参数的使用，并形成统一的流程。

`gg` 便是 git helper 的入口。

```sh
# 查看 git helper 版本号
gg version

# clone 时同时给出 upstream、origin 地址，会统一进行一次远程仓库设置
gg clone https://mygitlab.com/org_name/project_name --forked https://mygitlab.com/user_name/project_name

# 从 upstream/pre_release 分支拉出 feat_branch，并会设置 feat_branch 的远程分支追踪分支
gg checkout -r pre_release feat_branch

# 会同步 upstream/pre_release 的代码
gg pull

# 同 git add
gg add .

# 同 git commit
gg commit

# 同 git status
gg status

# 推送时，会自动推送到 origin
gg push
```

gg 只是将 git 的多条命令合并为一次命令操作。因此可以兼容原来 git 的流程。

## 安装

在 [release 页面][download_url] 中可以找到最新的版本。下载后解压到 `C:/Windows/System32` 或 `PATH` 环境变量所指的目录中。目前只在 bash 中测试通过。

> 安装 git for windows，可以同时安装 bash 环境。

[download_url]: https://codeberg.org/yuekcc/git_helper/releases

## LICENSE

MIT
