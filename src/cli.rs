use argh::FromArgs;

/// LazyGit，一个简单的 git 命令包装
#[derive(FromArgs)]
pub struct Cli {
    #[argh(subcommand)]
    pub commands: GitCommand,
}

#[derive(FromArgs)]
#[argh(subcommand)]
pub enum GitCommand {
    Add(AddCommand),
    Checkout(CheckoutCommand),
    Clone(CloneCommand),
    Commit(CommitCommand),
    Pull(PullCommand),
    Push(PushCommand),
    Status(StatusCommand),
    Sync(SyncCommand),
    Version(VersionCommand),
    Wip(WipCommand),
}

/// Clone 一个仓库
#[derive(FromArgs)]
#[argh(subcommand, name = "clone")]
pub struct CloneCommand {
    /// 仓库地址
    #[argh(positional)]
    pub repo_url: String,

    /// fork 出来的仓库地址
    #[argh(option, long = "forked")]
    pub forked_repo_url: Option<String>,
}

/// Pull 远程分支，同步代码
#[derive(FromArgs)]
#[argh(subcommand, name = "pull")]
pub struct PullCommand {
    /// 远程分支
    #[argh(positional)]
    pub remote_branch_name: Option<String>,
}

/// Checkout 新的分支
#[derive(FromArgs)]
#[argh(subcommand, name = "checkout")]
pub struct CheckoutCommand {
    /// 设置追踪的分支名称
    #[argh(option, short = 'r', long = "remote")]
    pub remote: Option<String>,

    /// 分支名称
    #[argh(positional)]
    pub branch_name: String,
}

/// Push 分支
#[derive(FromArgs)]
#[argh(subcommand, name = "push")]
pub struct PushCommand {
    /// 分支名称
    #[argh(positional)]
    pub branch_name: Option<String>,
}

/// Add 变更到缓存区
#[derive(FromArgs)]
#[argh(subcommand, name = "add")]
pub struct AddCommand {
    /// 文件/目录
    #[argh(positional)]
    pub files: Vec<String>,
}

/// Commit 提交变更
#[derive(FromArgs)]
#[argh(subcommand, name = "commit")]
pub struct CommitCommand {
    /// 文件/目录
    #[argh(option, short = 'm')]
    pub msg: Option<String>,
}

/// Status 查看分支状态
#[derive(FromArgs)]
#[argh(subcommand, name = "status")]
pub struct StatusCommand {}

/// Version 显示版本号
#[derive(FromArgs)]
#[argh(subcommand, name = "version")]
pub struct VersionCommand {}

/// Sync 同步仓库
#[derive(FromArgs)]
#[argh(subcommand, name = "sync")]
pub struct SyncCommand {
    /// 清理本地仓库
    #[argh(switch, short = 'p', long = "prune")]
    pub should_prune: bool,
}

/// Wip 增加为 WIP
#[derive(FromArgs)]
#[argh(subcommand, name = "wip")]
pub struct WipCommand {
    /// 推送到远程仓库
    #[argh(switch, short = 'p', long = "push")]
    pub should_push: bool,
}
