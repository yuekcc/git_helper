use std::fmt::Display;

use log::debug;
use regex::Regex;
use serde::{Deserialize, Serialize};
use xshell::{cmd, Cmd, Shell};

#[allow(dead_code)]
pub const REMOTE_ORIGIN: &str = "origin";

#[allow(dead_code)]
pub const REMOTE_CENTER: &str = "upstream";

#[derive(Deserialize, Serialize)]
pub struct TrackedBranch {
    pub remote: String,
    pub branch: String,
}

impl Display for TrackedBranch {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "TrackedBranch(remote = {}, branch = {})", &self.remote, &self.branch)
    }
}

fn quiet_run(cmd: Cmd) -> anyhow::Result<()> {
    #[cfg(debug_assertions)]
    cmd.run()?;

    #[cfg(not(debug_assertions))]
    cmd.quiet().run()?;

    Ok(())
}

pub fn check_version(sh: &Shell) -> anyhow::Result<()> {
    quiet_run(cmd!(sh, "git version"))?;

    Ok(())
}

fn fetch_all(sh: &Shell) -> anyhow::Result<()> {
    debug!("#fetch_all");
    quiet_run(cmd!(sh, "git fetch --all --tags"))?;

    Ok(())
}

fn get_project_name(url: &str) -> Option<String> {
    url.split('/')
        .last()
        .map(|s| s.trim_end_matches(".git"))
        .map(|s| s.to_string())
}

pub fn ensure_installed() {
    debug!("#ensure_installed");
    which::which("git").unwrap_or_else(|_| panic!("未发现可用的 git 命令"));
}

fn add_upstream(sh: &Shell, name: &str, url: &str) -> anyhow::Result<()> {
    debug!("#add_upstream, url = {}", url);
    quiet_run(cmd!(sh, "git remote add {name} {url}"))?;

    Ok(())
}

fn set_upstream(sh: &Shell, name: &str, url: &str) -> anyhow::Result<()> {
    debug!("#set_upstream, name = {}, url = {}", name, url);
    quiet_run(cmd!(sh, "git remote set-url {name} {url}"))?;

    Ok(())
}

pub fn clone(sh: &Shell, url: &str, forked_url: &str) -> anyhow::Result<()> {
    let project_name = get_project_name(url).unwrap_or_else(|| panic!("无法从 url 解析项目名称，url = {}", url));

    debug!("#clone, url = {}, project_name = {}", url, &project_name);

    // clone
    quiet_run(cmd!(sh, "git clone {url} {project_name}"))?;
    sh.change_dir(project_name);

    // 设置 remote
    debug!("#clone, set remote {} = {}", REMOTE_CENTER, url);
    add_upstream(sh, REMOTE_CENTER, url)?;
    debug!("#clone, set remote {} = {}", REMOTE_ORIGIN, url);
    set_upstream(sh, REMOTE_ORIGIN, forked_url)?;

    // update once
    fetch_all(sh)?;

    Ok(())
}

pub fn checkout(sh: &Shell, new_branch: &str, remote_branch: Option<String>) -> anyhow::Result<()> {
    fetch_all(sh)?;

    if let Some(remote_branch) = remote_branch {
        let remote = REMOTE_CENTER;
        debug!(
            "#checkout, remote = {}, remote_branch = {}, new_branch = {}",
            &remote, &remote_branch, new_branch
        );
        quiet_run(cmd!(sh, "git checkout -b {new_branch} {remote}/{remote_branch}"))?;
    } else {
        debug!("#checkout, just checkout new branch, name = {}", new_branch);
        quiet_run(cmd!(sh, "git checkout -b {new_branch}"))?;
    }
    // quiet_run(cmd!(sh, "git branch -u {REMOTE_ORIGIN}/{new_branch}"))?;

    Ok(())
}

pub fn pull(sh: &Shell, remote: &str, branch_name: &str) -> anyhow::Result<()> {
    debug!("#pull, remote = {}, branch_name = {}", remote, branch_name);
    quiet_run(cmd!(sh, "git pull {remote} {branch_name}"))?;

    Ok(())
}

pub fn get_current_branch_name(sh: &Shell) -> String {
    debug!("#get_current_branch_name");

    let out = cmd!(sh, "git branch --show-current")
        .output()
        .unwrap_or_else(|_| panic!("无法执行 git branch --show-current"));

    let name = std::str::from_utf8(&out.stdout).unwrap().trim();
    debug!("#get_current_branch_name, result = {}", name);

    String::from(name)
}

pub fn get_current_tracked_branch(sh: &Shell) -> Option<TrackedBranch> {
    debug!("#get_current_tracked_branch");
    let out = cmd!(sh, "git branch -vv")
        .output()
        .unwrap_or_else(|_| panic!("无法执行 git branch -vv"));
    let text = std::str::from_utf8(&out.stdout)
        .unwrap()
        .trim()
        .split('\n')
        .find(|line| line.trim_start().starts_with('*'))
        .unwrap_or_else(|| panic!("无法解释 git branch -vv 输出，未支持的格式"));

    //* main 4b068bb [origin/main: head 1] 改名
    let re_with_ahead = Regex::new(r".+\[(.+)/(.+):.+\].+").unwrap();
    //* main 4b068bb [origin/main] 改名
    let re_without_ahead = Regex::new(r".+\[(.+)/(.+)].+").unwrap();

    if re_with_ahead.is_match(text) {
        let captures = re_with_ahead.captures(text).unwrap();
        let result = TrackedBranch {
            remote: captures[1].to_string(),
            branch: captures[2].to_string(),
        };
        debug!("#get_current_tracked_branch, result = {}", &result);
        Some(result)
    } else if re_without_ahead.is_match(text) {
        let captures = re_without_ahead.captures(text).unwrap();
        let result = TrackedBranch {
            remote: captures[1].to_string(),
            branch: captures[2].to_string(),
        };
        debug!("#get_current_tracked_branch, result = {}", &result);
        Some(result)
    } else {
        debug!("#get_current_tracked_branch, result = NOTHING");
        None
    }
}

fn set_branch_message(sh: &Shell, branch_name: &str, msg: &str) -> anyhow::Result<()> {
    debug!("#set_branch_message, branch_name = {}, message = {}", branch_name, msg);

    let config_key = format!("branch.{}.description", branch_name);
    quiet_run(cmd!(sh, "git config {config_key} {msg}"))?;

    Ok(())
}

pub fn set_tracking_message(sh: &Shell, branch_name: &str, remote_branch: Option<String>) -> anyhow::Result<()> {
    let config = if let Some(remote_branch) = remote_branch {
        TrackedBranch {
            remote: REMOTE_CENTER.to_string(),
            branch: remote_branch,
        }
    } else {
        let current_branch = get_current_branch_name(sh);
        TrackedBranch {
            remote: REMOTE_ORIGIN.to_string(),
            branch: current_branch,
        }
    };
    let msg = serde_qs::to_string(&config)?;
    set_branch_message(sh, branch_name, &msg)?;

    Ok(())
}

fn get_branch_message(sh: &Shell, branch_name: &str) -> Option<String> {
    debug!("#get_branch_message, branch_name = {}", branch_name);

    let config_key = format!("branch.{}.description", branch_name);
    let out = cmd!(sh, "git config --get {config_key}").output();

    match out {
        Ok(output) => {
            let msg = std::str::from_utf8(&output.stdout).unwrap().trim();
            debug!("#get_branch_message, result = {}", msg);
            Some(String::from(msg))
        }
        Err(_) => {
            debug!("#get_branch_message, result = NOTHING");
            None
        }
    }
}

pub fn get_tracking_message(sh: &Shell, branch_name: &str) -> Option<TrackedBranch> {
    let msg = get_branch_message(sh, branch_name);
    msg.map(|text| serde_qs::from_str::<TrackedBranch>(&text).unwrap())
}

pub fn add(sh: &Shell, files: Vec<String>) -> anyhow::Result<()> {
    let list = &files.join(" ");
    debug!("#add, files = {}", list);
    quiet_run(cmd!(sh, "git add {list}"))?;

    Ok(())
}

pub fn commit(sh: &Shell, msg: Option<String>) -> anyhow::Result<()> {
    let _cmd = if let Some(msg) = msg {
        debug!("#commit, with inline message, message = {}", &msg);
        cmd!(sh, "git commit -m {msg}")
    } else {
        debug!("#commit, without inline message");
        cmd!(sh, "git commit")
    };
    quiet_run(_cmd)?;

    Ok(())
}

pub fn push(sh: &Shell, branch_name: Option<String>) -> anyhow::Result<()> {
    let _cmd = if let Some(branch_name) = branch_name {
        debug!("#push, branch_name = {}", &branch_name);
        cmd!(sh, "git push -u {REMOTE_ORIGIN} {branch_name}")
    } else {
        debug!("#push, without branch_name");
        let current_branch = get_current_branch_name(sh);

        if let Some(tracked_branch) = get_current_tracked_branch(sh) {
            // 如果不是当前分支，设置追踪分支
            if current_branch != tracked_branch.branch {
                cmd!(sh, "git push -u {REMOTE_ORIGIN} {current_branch}")
            } else {
                cmd!(sh, "git push")
            }
        } else {
            cmd!(sh, "git push -u {REMOTE_ORIGIN} {current_branch}")
        }
    };

    quiet_run(_cmd)?;

    Ok(())
}

pub fn status(sh: &Shell) -> anyhow::Result<()> {
    debug!("#status");
    quiet_run(cmd!(sh, "git status"))?;

    Ok(())
}

pub fn sync(sh: &Shell, should_prune: bool) -> anyhow::Result<()> {
    debug!("#sync, should_prune = {}", should_prune);

    let _cmd = if should_prune {
        cmd!(sh, "git fetch --all --tags --prune")
    } else {
        cmd!(sh, "git fetch --all --tags")
    };

    quiet_run(_cmd)?;
    Ok(())
}

pub fn add_wip(sh: &Shell, should_push: bool) -> anyhow::Result<()> {
    debug!("#wip, should_push = {}", should_push);

    add(sh, vec![".".to_string()])?;
    commit(sh, Some("WIP".to_string()))?;
    if should_push {
        push(sh, None)?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_re_branch_vv() {
        let text = "* main 4b068bb [origin/main] 改名";
        let re = Regex::new(r".+\[(.+)/(.+)].+").unwrap();
        let captures = re.captures(text);
        if let Some(cap) = captures {
            println!("0: {} 1: {} 2: {}", &cap[0], &cap[1], &cap[2]);
        }
    }

    #[test]
    fn test_pick_branch_vv_with_ahead() {
        let re = Regex::new(r".+\[(.+)/(.+):.+\].+").unwrap();
        let text = "* main 3aa6415 [origin/main: ahead 1] 可以获取当前追踪的远程分支";
        let captures = re.captures(text);
        if let Some(cap) = captures {
            println!("0: {} 1: {} 2: {}", &cap[0], &cap[1], &cap[2]);
        }
    }
}
