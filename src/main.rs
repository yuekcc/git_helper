use log::info;
use xshell::Shell;

mod cli;
mod git;
use crate::cli::{Cli, GitCommand};

fn main() -> anyhow::Result<()> {
    #[cfg(debug_assertions)]
    simple_logger::init_with_level(log::Level::Debug).unwrap();

    #[cfg(not(debug_assertions))]
    simple_logger::init_with_env().unwrap();

    let cli: Cli = argh::from_env();
    let sh = Shell::new()?;

    git::ensure_installed();

    match cli.commands {
        GitCommand::Clone(cmd) => {
            let forked_repo_url = cmd.forked_repo_url.unwrap_or_else(|| cmd.repo_url.clone());
            git::clone(&sh, &cmd.repo_url, &forked_repo_url)?;

            // clone 出来的第一个分支，需要先设置一下备注
            let current_branch = git::get_current_branch_name(&sh);
            git::set_tracking_message(&sh, &current_branch.clone(), Some(current_branch))?;
        }
        GitCommand::Checkout(cmd) => {
            let remote_branch = cmd.remote;

            git::checkout(&sh, &cmd.branch_name, remote_branch.clone())?;
            git::set_tracking_message(&sh, &cmd.branch_name, remote_branch)?;
        }
        GitCommand::Pull(cmd) => {
            if let Some(branch_name) = cmd.remote_branch_name {
                git::pull(&sh, git::REMOTE_CENTER, &branch_name)?;
                return Ok(());
            }

            let current_branch = git::get_current_branch_name(&sh);
            let config = git::get_tracking_message(&sh, &current_branch);

            if let Some(_config) = config {
                git::pull(&sh, &_config.remote, &_config.branch)?;
            } else if let Some(current_tracking) = git::get_current_tracked_branch(&sh) {
                git::pull(&sh, &current_tracking.remote, &current_tracking.branch)?;
            } else {
                info!("no tracking branch, setup first");
            }
        }
        GitCommand::Push(cmd) => {
            git::push(&sh, cmd.branch_name)?;
        }
        GitCommand::Add(cmd) => {
            git::add(&sh, cmd.files)?;
        }
        GitCommand::Commit(cmd) => {
            git::commit(&sh, cmd.msg)?;
        }
        GitCommand::Status(_) => {
            git::status(&sh)?;
        }
        GitCommand::Version(_) => {
            println!("lg version {}", env!("CARGO_PKG_VERSION"));
            git::check_version(&sh)?;
        }
        GitCommand::Sync(cmd) => {
            git::sync(&sh, cmd.should_prune)?;
        }
        GitCommand::Wip(cmd) => {
            git::add_wip(&sh, cmd.should_push)?;
        }
    }

    Ok(())
}
